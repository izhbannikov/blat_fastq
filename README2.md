To use FastQ file as a input:

blat database query [-ooc=11.ooc] output.psl -fastq

By default input format file is set to FastA.
